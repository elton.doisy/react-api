const User = require('../models/userModel');
const jwt = require("jsonwebtoken");
const bcrypt = require('bcrypt');

exports.userRegister = (req, res) => {

    
    bcrypt.genSalt(10, function(err, salt) {
            bcrypt.hash(req.body.password, salt, function(err, hash) {
                
                let newUser = new User({email: req.body.email,password: hash});

                newUser.save((error, user) => {
                    if (error) {
                        res.status(401);
                        console.log(error);
                        res.json({ message: "Reqûete invalide." });
                    }
                    else {
                        res.status(201);
                        res.json({ message: `Utilisateur crée : ${user.email}` });
                    }
                })
        });
    });
    
}

exports.loginRegister = (req, res) => {
    // Find user
    User.findOne({ email: req.body.email }, (error, user) => {
        // If user not found
        if (error) {
            res.status(500);
            console.log(error);
            res.json({ message: "Utilisateur non trouvé" });
        }
        else {
            
            bcrypt.compare(req.body.password, user.password, function(err, result) {
                if (err) {
                    res.status(401);
                    console.log(error);
                    res.json({ message: "Email ou Mot de passe incorrect" });
                }else{
                    let userData = {
                        id: user._id,
                        email: user.email,
                        role: "admin"
                    }
                    jwt.sign(userData, process.env.JWT_KEY, { expiresIn: "30 days" }, (error, token) => {
                        if(error) {
                            res.status(500);
                            console.log(error);
                            res.json({message: "Impossible de générer le token"});
    
                        }
                        else {
                            res.status(200);
                            res.json({token});
                        }
                    })
                }
                
            });
        }
    })
}